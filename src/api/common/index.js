import {
    $get,
    $post
  } from '@/utils/http'
  
  export default {
    // 获取图片基础路径
    getIp() {
      return $post('/common/serverUrl')
    },
    //图片上传接口
    uploadImage(params) {
      return $post('/common/image-upload', params)
    },
    // 文件上传接口
    uploadText(params) {
      return $post('/common/file-upload', params)
    },
    //举报平台背景图片
    ReportBGPic(params) {
      return $post('/portal/websit/complainSeting/queryComplainOnLine', params)
    }
  }
  
  
  