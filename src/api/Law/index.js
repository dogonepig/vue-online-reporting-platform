import {
    $get,
    $post
} from '@/utils/http'
//法律法规
export default {
    //查询法律法规列表
    getLawList(params) {
        return $post('/portal/supervise/complain/regulation-page', params)
    },
    //查询法律法规详情
    getLawDetails(params) {
        return $post('/portal/supervise/complain/regulation-detail', params)
    },
}