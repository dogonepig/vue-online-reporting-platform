import {
    $get,
    $post
} from '@/utils/http'
//网上举报
export default {
    //匿名举报图片获取
    getPicAnonymous(params) {
        return $get('/portal/supervise/complain/anonym/capthca', params,null,{ responseType: 'arraybuffer' })
    },
    //实名举报图片获取
    getPicSignde(params) {
        return $get('/portal/supervise/complain/real-name/capthca', params,null,{ responseType: 'arraybuffer' })
    },
    //匿名举报须知接口
    getReportKnow(params) {
        return $get('/portal/supervise/complain/notice-detail', params)
    },
    // 获取单位列表
    getReportCompanyList(params) {
        return $get('/portal/supervise/complain/company-list', params)
    },
    // 获取举报人姓名列表
    getReportNameList(params) {
        return $post('/portal/supervise/complain/person-list', params)
    },
    // 问题类型和细类下拉
    getReportQuestion(params) {
        return $get('/common/dict/item-list', params)
    },
    // 匿名举报提交表单接口
    PushReportForm(params) {
        return $post('/portal/supervise/complain/anonym/add', params)
    },
    // 实名举报提交表单接口
    PushSigndeReportForm(params) {
        return $post('/portal/supervise/complain/real-name/add', params)
    },
}