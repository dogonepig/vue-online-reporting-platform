// 法律法规
import Law from './Law'
// 网上举报
import ReportInternet from './ReportInternet'
//举报调查
import Investigation from './Investigation'
import common from './common'
const Api = Object.assign({},Law,ReportInternet,Investigation,common)
export default Api



