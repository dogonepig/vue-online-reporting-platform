import {
    $get,
    $post
} from '@/utils/http'
//jubao调查
export default {
    //查询图片
    getPicQuery(params) {
        return $get('/portal/supervise/complain/progress/capthca', params,null,{ responseType: 'arraybuffer' })
    },
    //查询举报详情
    getQueryDetail(params) {
        return $post('/portal/supervise/complain/progress/detail', params)
    },

}