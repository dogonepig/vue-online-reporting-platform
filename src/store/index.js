import Vue from 'vue'
import Vuex from 'vuex'
import api from "@/api"
Vue.use(Vuex)

export var store = new Vuex.Store({
    state: {
        fileIp: "",
    },
    mutations: {
        SET_FILEIP(state, fileIp) {
            state.fileIp = fileIp
            // console.log(state.fileIp,222)
        },
    },
    actions: {
        SET_FILEIP({ commit }) {
            return new Promise((resolve) => {
                api.getIp().then(res => {
                    commit('SET_FILEIP', res.data)
                    resolve();
                });
            })
        },

    }
})


