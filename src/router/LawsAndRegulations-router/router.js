import LawsAndRegulations from "@/views/LawsAndRegulations/index.vue"
export default [
  {
        path: "*",
        redirect: "/LawsAndRegulations"
      },
    {
        name: "LawsAndRegulations",
        path: "/LawsAndRegulations",
        component: LawsAndRegulations
      },
]