import ReportInvestigation from "@/views/ReportInvestigation/index.vue"
import Query from "@/views/ReportInvestigation/Query/Query.vue"
import Result from "@/views/ReportInvestigation/Result/Result.vue"
export default [{
  name: "ReportInvestigation",
  path: "/ReportInvestigation",
  component: ReportInvestigation,
  redirect: "/ReportInvestigation/Query",
  children: [{
      name: "Query",
      path: "Query",
      component: Query,
    },
    {
      name: "Result",
      path: "Result",
      component: Result,
    },
  ]
}, ]