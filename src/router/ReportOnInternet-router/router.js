
import ReportOnInternet from "@/views/ReportOnInternet/index.vue"
import Tips from "@/views/ReportOnInternet/Tips.vue"
import Anonymous from "@/views/ReportOnInternet/Anonymous.vue"
//实名认证
import SigndeReport from "@/views/ReportOnInternet/SigndeReport/SigndeReport.vue"
//实名认证确认
import Authentication from "@/views/ReportOnInternet/SigndeReport/Authentication.vue"
//查询码
import InquiryCode from "@/views/ReportOnInternet/SigndeReport/InquiryCode.vue"
//举报表单填写页面
import Sign from "@/views/ReportOnInternet/SigndeReport/Sign.vue"
export default [{
  name: "ReportOnInternet",
  path: "/ReportOnInternet",
  component: ReportOnInternet,
  redirect: "/ReportOnInternet/Tips",
  children: [{
    name: "Tips",
    path: "Tips",
    component: Tips,
  },
  {
    name: "Anonymous",
    path: "Anonymous",
    component: Anonymous,
  },
  {
    name: "SigndeReport",
    path: "SigndeReport",
    component: SigndeReport,
  },
  {
    name: "Authentication",
    path: "Authentication",
    component: Authentication,
  },
  {
    name: "InquiryCode",
    path: "InquiryCode",
    component: InquiryCode,
  },
    {
      name: "Sign",
      path: "Sign",
      component: Sign ,
    },
  ]
},]