import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

let routes = [];
(modules => modules.keys().forEach((key) => {
  routes = routes.concat(modules(key).default);
}))(require.context('@/router/', true, /router\.js$/));

export default new VueRouter({ routes,mode:"history" });
// 双击报错
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};
