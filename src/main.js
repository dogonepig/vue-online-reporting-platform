import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import './assets/css/global.css'
import axios from 'axios'
import { store } from "@/store/index.js"
//粘贴板
import VueClipboard from 'vue-clipboard2'
// api
import Api from '@/api'
//富文本
import VueQuillEditor from 'vue-quill-editor';
//  require styles 引入样式
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
//富文本编辑器
Vue.use(VueQuillEditor)
Vue.use(VueClipboard)
Vue.prototype.Api = Api

Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.use(ElementUI);
new Vue({
  router, store,
  render: h => h(App)
}).$mount('#app')