# 项目名称-网上举报平台
# 项目文件结构
1,dist -打包文件

2，node_modules

3，public

4，src

4,api -接口文件

4,2assets静态资源

4.21css 样式

4.22image 图片文件

4,3components -通用组件，页头页脚，轮播图组件，分页器组件等

4.4 views页面

4,4，1，法律法规 views/LawsAndRegulations

442网上举报 views/ReportOnInternet

443匿名举报 views/ReportOnInternet/Anonymous

444实名举报 views/ReportOnInternet/SigndeReport

5举报调查 views/ReportInvestigation

51实名认证 views/ReportInvestigation/query

52举报结果查询 views/ReportInvestigation/result

5router -路由文件

6，store-vuex文件-图片ip地址

7，styles样式

8，utils全局方法
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
