module.exports = {
    devServer: {
        port: 8888,
        open: true,
        proxy: {
            "/api": {
                target: "http://192.168.2.34:8080/",
                changeOrign: true,
                pathRewrite: {
                    "^/api": ""
                }
            }
        }
    },
    lintOnSave: false//彻底关闭eslint
}
